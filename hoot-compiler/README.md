### Hoot Compiler Library

Contains the ANTLR grammar, language element classes, and code generation templates for the Hoot compiler.
Also, some sample Hoot code and tests for the compiler to ensure it operates properly.

| **Package** | **Contents** |
| ----------- | ------------ |
| Hoot Compiler | Hoot compiler command line interface |
| Hoot Compiler Constants | Hoot language literals: numbers, arrays, string, symbols |
| Hoot Compiler Expressions | Hoot language expressions |
| Hoot Compiler Notes | Hoot language annotations and decorations |
| Hoot Compiler Scopes | Hoot language scopes: blocks, methods, signatures, faces, files |


```
Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
```
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for LICENSE details.
