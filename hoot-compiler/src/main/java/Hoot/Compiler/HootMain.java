package Hoot.Compiler;

import java.io.*;
import java.util.*;
import org.apache.commons.cli.*;
import org.apache.commons.cli.Option.*;
import static org.apache.commons.io.FileUtils.*;

import Hoot.Runtime.Faces.*;
import Hoot.Runtime.Maps.Package;

import static Hoot.Runtime.Faces.Logging.*;
import static Hoot.Runtime.Maps.Package.*;
import static Hoot.Compiler.Scopes.File.*;
import static Hoot.Runtime.Maps.ClassPath.*;
import static Hoot.Runtime.Names.Primitive.*;
import static Hoot.Runtime.Functions.Utils.*;
import static Hoot.Runtime.Maps.Library.CurrentLib;
import static Hoot.Runtime.Functions.Exceptional.*;

/**
 * Compiles Hoot code to Java classes and types.
 * Takes command line arguments and thereby instructs the compiler.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class HootMain implements Logging {

    private HootMain() { }

    /**
     * Compiles the Hoot files located in the configured source folder.
     * @param args command line arguments
     */
    public static void main(String[] args) { HootMain m = new HootMain(); m.parseCommand(args); m.runCommand(); }
    private void runCommand() { if (this.needsHelp()) printHelp(); else compilePackages(); }

    private final List<String> packageList = new ArrayList();
    private void compilePackages() {
        report(Empty); prepareFolders(); loadPaths(); packageList.addAll(packageList());
        if (packageList.isEmpty()) compilePackagesUnder(sourceFolder, targetFolder); else compileAllPackages(); }

    private CommandLine command;
    private final CommandLineParser parser = new DefaultParser();
    private void parseCommand(String[] args) { command = nullOrTryLoudly(() -> parser.parse(availableOptions(), args)); }

    static final String Compiler = "hoot-compiler";
    private void printHelp() { new HelpFormatter().printHelp(Compiler, availableOptions()); }
    private boolean needsHelp() { return falseOr((c) -> c.hasOption(Help), command); }
    private boolean cleansFolders() { return falseOr((c) -> c.hasOption(Clean), command); }

    static final String BasePath = "user.dir";
    private String basePath() { return emptyOr((c) -> c.getOptionValue(Base, systemValue(BasePath)), command); }

    static final String SourcePath = "src/main";
    private String sourcePath() { return emptyOr((c) -> c.getOptionValue(Source, SourcePath), command); }

    static final String TargetPath = "src/main/java";
    private String targetPath() { return emptyOr((c) -> c.getOptionValue(Folder, TargetPath), command); }
    private List<String> packageList() { return wrap(command.getOptionValues(Packages)); }

    private Options availableOptions() {
        Options result = new Options();
        result.addOption(baseOption());
        result.addOption(sourceOption());
        result.addOption(targetOption());
        result.addOption(packageOption());
        result.addOption(cleanOption());
        result.addOption(helpOption());
        return result;
    }

    private String basePath;
    private File baseFolder;
    private File sourceFolder;
    private File targetFolder;
    private File classFolder;
    static final String Binary = "binary";
    private void prepareFolders() {
        // validate the paths
        basePath = basePath();
        baseFolder = locate(BasePath, basePath);
        if (baseFolder == null) return;

        sourceFolder = locate(Source, sourcePath());
        targetFolder = locate(Target, targetPath(), TargetPath);
        classFolder  = locate(Binary, targetPath(), ClassesPath);
    }
    
    static final String Adding = "adding";
    static final String RelativePrefix = "../";
    static final String ClassesPath = "target/classes";
    private final List<File> basicPaths = new ArrayList();
    private void addPath(String relativePath, String... qualifiers) { 
        if (hasAny(qualifiers)) {
            if (basePath.endsWith(qualifiers[0])) {
                basicPaths.add(locate(Adding, RelativePrefix + relativePath, ClassesPath)); 
            }
        }
        else {
            basicPaths.add(locate(Adding, RelativePrefix + relativePath, ClassesPath)); 
        }
    }
    
    private void loadPaths() {
        File[] paths = { sourceFolder, targetFolder, classFolder };
        basicPaths.addAll(wrap(paths));
        addPath("libs-hoot", "code-samples");
        addPath("libs-smalltalk", "code-hoot");
        addPath("hoot-runtime");
        addPath("hoot-abstracts");
        CurrentPath.loadPaths(basicPaths);
        UnitFactory = StandardUnitFactory;
    }

    static final String Blank = " ";
    static final String FolderFound = "%s folder = %s";
    static final String FolderFailed = "can't locate %s";
    private File locate(String pathName, String... folderPaths) {
        File result = new File(folderPaths[0]);

        if (!BasePath.equals(pathName)) {
            pathName = Blank + Blank + pathName;
            if (!result.isAbsolute()) {
                result = new File(baseFolder.getAbsolutePath(), folderPaths[0]);
                if (folderPaths.length > 1) {
                    result = new File(result.getAbsolutePath(), folderPaths[1]);
                }
            }
        }

        if (result.exists()) {
            report(format(FolderFound, pathName, result.getAbsolutePath()));
            return result;
        }
        else if (Target.equals(pathName)) {
            result.mkdirs();
            return result;
        }
        else {
            error(format(FolderFailed, result.getAbsolutePath()));
            return null;
        }
    }

    private void compileAllPackages() {
        packageList.forEach((packageName) -> compilePackageNamed(packageName)); }

    static final String[] Skipped = { "resources", "java" };
    private static boolean isFolder(File candidate) { return candidate.isDirectory(); }
    private void compilePackagesUnder(File hootFolder, File javaFolder) {
        List<String> skippedFolders = wrap(Skipped);
        compileOnly(hootFolder, javaFolder);

        File[] subFolders = hootFolder.listFiles((File candidate) -> isFolder(candidate));
        for (File folder : subFolders) {
            String folderName = folder.getName();
            if (!skippedFolders.contains(folderName)) {
                File resultFolder = new File(javaFolder, folderName);
                compilePackagesUnder(new File(hootFolder, folderName), resultFolder);
            }
        }
    }

//    static final String JavaFileType = ".java";
    static final String HootFileType = ".hoot";
    private static boolean isHoot(File f) { return f.isFile() && f.getPath().endsWith(HootFileType); }
    private void compileOnly(File hootFolder, File javaFolder) {
        File[] hootFiles = hootFolder.listFiles((File candidate) -> isHoot(candidate));
        if (hootFiles.length > 0) {
            String packageName = Package.nameFrom(targetFolder, javaFolder);
            if (packageList.isEmpty() || packageList.contains(packageName)) {
                compilePackageNamed(packageName);
            }
        }
    }

    static final String Cleaning = "cleaning %s";
    static final String Translation = "translating %s";
    private void compilePackageNamed(String packageName) {
        report(Empty);
        cleanUp(packageName);
        report(format(Translation, packageName));
        runLoudly(() -> { // report any throwable, as that's seriously jacked! --nik
            Map<String, UnitFile> fileMap = Package.named(packageName).parseSources();
            CurrentLib.loadEmptyPackages();
            fileMap.values().forEach(f -> f.compile());
        });
    }

    private void cleanUp(String packageName) {
        File javaFolder = Package.named(packageName).targetFolder();
        if (javaFolder.exists() && cleansFolders()) {
            report(format(Cleaning, packageName));
            deleteQuietly(javaFolder);
        }

        if (!javaFolder.exists()) {
            javaFolder.mkdirs();
        }
    }

    static final String Help = "help";
    private Option helpOption() { return buildOption(Help, "displays this help").build(); }

    static final String Base = "base";
    private Option baseOption() { return buildOption(Base, Base+Path, "base folder path").hasArg().build(); }

    static final String Source = "source";
    private Option sourceOption() { return buildOption(Source, Source+Path, "Hoot sources folder path").hasArg().build(); }

    static final String Path = "Path";
    static final String Folder = "folder";
    static final String Target = "target";
    private Option targetOption() { return buildOption(Folder, Target+Path, "target Java folder path").hasArg().build(); }

    static final String Clean = "clean";
    private Option cleanOption() { return buildOption(Clean, "removes any previously generated code").build(); }

    static final char BLANK = ' ';
    static final String Packages = "packages";
    static final String PackNames = "packageNames";
    private Option packageOption() {
        return buildOption(Packages, PackNames, "packages to compile").valueSeparator(BLANK).hasArgs().build(); }

    static final String Dash = "-";
    static final String Optional = Dash + Dash;
    private static String shortened(String optionName) { return Dash + shortOption(optionName); }
    private static String shortOption(String optionName) { return optionName.substring(0, 1); }
    private Builder buildOption(String optionName, String text) { return buildOption(optionName, Empty, text); }
    static final String Optioned = "Option";
    private Builder buildOption(String optionName, String argName, String text) {
        return Option.builder(shortOption(optionName))
                .longOpt(optionName).required(false).desc(text)
                .argName(argName.isEmpty() ? optionName+Optioned : argName); }


    /**
     * @param folderPath a folder path
     * @param packages a package name list
     * @return arguments for a clean compiler command
     */
    public static String[] buildCleanCommand(String folderPath, String... packages) {
        return buildCommand(folderPath, packages, Optional + Clean); }

    /**
     * @param folderPath a folder path
     * @param packages a package name list
     * @param options some compiler option(s)
     * @return arguments for a compiler command
     */
    public static String[] buildCommand(String folderPath, String[] packages, String... options) {
        List<String> results = new ArrayList();
        String packageList = wrap(packages).toString();
        packageList = strip(packageList, Bounds).replace(Comma, Empty);

        String[] args = {
            shortened(Folder),
            folderPath,
            shortened(Packages),
            packageList,
        };

        List<String> argList = wrap(args);
        List<String> optionList = wrap(options);

        results.addAll(argList);
        results.addAll(optionList);

        String[] empty = { };
        return unwrap(results, empty);
    }

    static final String Dot = ".";
    static final String Comma = ",";
    static final String Bounds = "[]";
    static final String Slash = "/";

} // HootMain
