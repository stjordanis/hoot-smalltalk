package Hoot.Compiler.Constants;

import java.util.*;
import Hoot.Runtime.Emissions.*;
import static Hoot.Runtime.Names.Keyword.*;
import static Hoot.Runtime.Emissions.Emission.*;

/**
 * A source of literal emissions.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public interface LiteralSource extends EmissionSource {

    default Emission emitArrayPrim(List<Emission> values) { return emit("Array").values(values); }
    default Emission emitArray(List<Emission> values) { return emit("ArrayWith").values(values); }
    default Emission emitNewArray(List<Emission> values) { return emit("NewArray").values(values); }
    default Emission emitNamedValue(String name, Emission type) { return emit("NamedValue").name(name).type(type); }
    default Emission emitScalar(String s, String factory) { return emit("Scalar").value(s).with("factory", factory); }
    default Emission emitBoolean(String value) { return emit(value.equals("true") ? "True" : "False"); }
    default Emission emitInteger(String value) { return emit(Integer).value(value); }

} // LiteralSource
