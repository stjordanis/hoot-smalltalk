package Hoot.Elements;

import org.junit.*;
import static org.junit.Assert.*;

import static Hoot.Compiler.HootMain.*;
import Hoot.Runtime.Behaviors.Signed;
import Hoot.Runtime.Behaviors.Typified;
import Hoot.Runtime.Faces.Logging;
import Hoot.Runtime.Maps.Library;

/**
 * Compiles a few sample classes.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
//@Ignore
public class CompilerTest implements Logging {
    
    static final String SampleCode = "../sample-code";
    static final String[] SamplePackages = {
        "Samples.Core",
        "Samples.Behaviors",
        "Samples.Magnitudes",
        "Samples.Geometry",
    };
    
    @Test public void helpSample() {
        String[] command = { "--help" };
        main(command);
    }

    @Test public void sampleCompile() { 
        main(buildCommand(SampleCode, SamplePackages, "-s", "src/test", "--clean"));
        Typified point = find("Samples.Geometry.Point");
        Typified subject = find("Samples.Core.Subject");
        Signed ps = point.getSigned("equals(Subject)");
        Signed ss = subject.getSigned("equals(Subject)");
        assertTrue(ps.overrides(ss));
        assertTrue(ps.overridesHeritage());
    }
    
    private Typified find(String faceName) { return Library.findFace(faceName); }

} // CompilerTest
