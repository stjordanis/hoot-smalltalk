### Hoot Examples

Contains code for the Hoot example classes located in [src/main/Hoot/Examples](src/main/Hoot/Examples).
[ExamplesTest](src/test/java/Test/Examples/ExamplesTest.java) runs during a build, and calls 
the Hoot compiler to generate Java classes in **libs-samples** for the Hoot classes contained in this library.

| **Test** | **Description** |
| CollectionsTest | confirms proper operation of the Hoot Collection classes |
| GeometryTest | confirms proper operation of the Hoot Geometry classes |
| HelloWorld | a simple Hello World example |
| IntegersTest | confirms proper operation of the Hoot Magnitude classes |
| LiteralsTest | confirms proper operation of the Hoot literal classes |
| MagnitudesTest | confirms proper operation of the Hoot Magnitude classes |
| SimpleHanoi | reports performance of transferring Hanoi tower disks |
| SticBenchmark | reports performance of samples derived from some STIC benchmarks |
| StreamsTest | confirms proper operation of the Hoot Stream classes |
| ThreadTest | confirms proper operation of Hoot integration with Java threads |


```
Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
```
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for LICENSE details.
