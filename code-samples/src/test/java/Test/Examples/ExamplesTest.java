package Test.Examples;

import org.junit.*;
import static Hoot.Compiler.HootMain.*;

/**
 * Compiles the Hoot example classes.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class ExamplesTest {
    
    static final String LibsSamples = "../libs-samples";
    static final String[] SamplePackages = { "Hoot.Examples", };
    @Test public void compileLibrary() { main(buildCleanCommand(LibsSamples, SamplePackages)); }

} // ExamplesTest
