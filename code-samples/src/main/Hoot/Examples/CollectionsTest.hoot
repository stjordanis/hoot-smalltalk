@Notice :'Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for more details.'!

Hoot Behaviors Class import.
Hoot Streams Transcript import.
Smalltalk Collections importAll.

Object subclass: CollectionsTest. "Regression tests collections."

CollectionsTest members: [

    @Static! Void! main: Java Lang String! ... args [ CollectionsTest basicNew testCollections. ]

    testCollections [
        self testString.
        self testArray.
        self testSort.
        self testSet.
        self testInterval.
        self testOrdered.
        self testBag.
        self testDictionary.
        self testIdDictionary.
        self testNames.
        self testHeritage.
    ]

    testString [
        string := 'aaaa' , 'bbbb'.
        Transcript log
            print: 'concat = '; printLine: string;
            print: 'upper =  '; printLine: string asUppercase;
            cr.
    ]

    Array! sample := #( 5 4 3 2 1 ).
    testArray [
        Transcript log print: 'array =  '.
        sample do: [ :each | 
            Transcript log print: each printString; space.
        ].
        Transcript log cr.
    ]

    testSet [
        Transcript log print: 'set =    '.
        s := Set new.
        s addAll: sample.
        s addAll: sample.
        s do: [ :each |
            Transcript log print: each printString; space.
        ].
        s removeAll: sample.
        Transcript log cr; print: 'size after removal = '; printLine: s size printString; cr.
    ]

    testSort [
        Transcript log print: 'sorted = '.
        sample asSortedCollection do: [ :each | 
            Transcript log print: each printString; space.
        ].
        Transcript log cr.
    ]

    testInterval [
        Transcript log print: 'range = ( 1 to: 5 ) = '.
        ( 1 to: 5 ) do: [ :each |
            Transcript log print: each printString; space.
        ].

        Transcript log cr; print: 'range = #( 1 2 3 4 5 ) ? '; 
            printLine: ( ( 1 to: 5 ) = sample ) printString.

        Transcript log print: 'range = #( 1 2 3 4 5 ) asOrderedCollection ? '; 
            printLine: ( ( 1 to: 5 ) = sample asOrderedCollection ) printString; cr.
    ]

    OrderedCollection! o := OrderedCollection new.
    testOrdered [
        Transcript log print: 'ordcol = '.
        o addAll: sample.
        o addAll: sample.
        o do: [ :each |
            Transcript log print: each printString; space.
        ].
        Transcript log cr.
    ]

    testBag [
        Transcript log print: 'bag =    '.
        CollectedBaggage! b := o asBag.
        b do: [ :each |
            Transcript log print: each printString; space.
        ].
        Transcript log cr.
    ]

    Dictionary! d := Dictionary new.
    testDictionary [
        Transcript log print: 'dict =   '; cr.
        sample do: [ :each | 
            d at: each printString put: each.
        ].
        d keysAndValuesDo: [ : String! key :value |
            Transcript log 
                print: 'key = '; print: key;
                print: ' value = '; print: value printString; cr.
        ].
        Transcript log cr.
    ]

    IdentityDictionary! id := IdentityDictionary new.
    testIdDictionary [
        id at: 123456789 put: 123456789.
        id at: 123456789 put: 123456789.
        id at: 1 put: 1.
        id at: 1 put: 1.
        Transcript log printLine: 'id dictionary size = ', id size printString; cr.
    ]

    Array! names := #( 'ddd' 'ccc' 'bbb' 'aaa' ).
    testNames [
        Transcript log print: 'names =  '.
        names do: [ :each | 
            Transcript log print: each printString; space.
        ].
        Transcript log cr; print: 'sorted = '.
        names asSortedCollection do: [ :each | 
            Transcript log print: each printString; space.
        ].
        Transcript log cr.
    ]

    testHeritage [
        Transcript log cr; printLine: 'hierarchy:'.
        Object type withAllSubclasses asSortedCollection do: [ : Behavior! aClass |
            Transcript log printLine: aClass name.
        ].
    ]
]
