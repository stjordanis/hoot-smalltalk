@Notice :'Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for more details.'!

Hoot Streams Transcript import.

Object subclass: MagnitudesTest. "Regression tests Smalltalk Magnitudes."

MagnitudesTest members: [

    @Static! Void! main: Java Lang String! ... args [ MagnitudesTest basicNew testAll. ]

    testAll [
        self 
            show: '[ 5 + 6 ]';
            show: '[ 5 - 6 ]';
            show: '[ 5 * 6 ]';
            show: '[ 5 / 6 ]';
            show: '[ 5 % 6 ]';

            show: '[ 5 ** 30 ]';
            show: '[ (5 - 6) abs ]';
            show: '[ (5 / 6) rounded ]';
            show: '[ (5 / 6) reciprocal ]';

            show: '[ 5 @ 6 ]';
            show: '[ 5 >> 6 ]';
            show: '[ (5 / 6) + (6 / 5) ]';
            show: '[ 20 squared sqrt ]';

            show: '[ 11.1234s5 + 3.14s4 ]';
            show: '[ $c asUppercase ]';
            show: '[ $9 isDigit ]';

            show: '[ Float type pi ]';
            show: '[ Float type pi asFraction ]';
            show: '[ Float type pi asFraction asFloat ]';

            show: '[ Double type pi ]';
            show: '[ Double type pi asFraction ]';
            show: '[ Double type pi asFraction asFloat ]';

            showPrim: '[ Time clockPrecision ]';

            yourself.

        Transcript log cr.
    ]

    show: String! code [ Transcript log print: code; print: ' = '; printLine: code evaluate printString. ]
    showPrim: String! code [ Transcript log print: code; print: ' = '; printLine: code evaluatePrimitively toString. ]
]
