@Notice :'Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for more details.'!

Hoot Magnitudes SmallInteger importStatics.
Hoot Runtime Names Primitive import.
Hoot Streams Transcript import.

Object subclass: IntegersTest. "Compares addition versus increment."

IntegersTest members: [

    @Static! Void! main: Java Lang String! ... args [ IntegersTest basicNew testIntegers. ]

    @Primitive! SmallInteger! fullweightSample [ ^SmallInteger from: (Primitive elementaryMaxShort + 2) ]

    SmallInteger! test.
    FastInteger! fast.

    testIntegers [
        test := 0 + 1.
        Transcript log report: '100000 flyweight  ops = ' 
        after: 100000 timesRepeat: [
            test := test - Unity.
            test := test + Unity.
        ].

        test := self fullweightSample.
        Transcript log report: '100000 fullweight ops = ' 
        after: 100000 timesRepeat: [
            test := test - Unity.
            test := test + Unity.
        ].

        fast := self fullweightSample asFastInteger.
        Transcript log report: '100000 fastweight ops = ' 
        after: 100000 timesRepeat: [
            fast += Unity.
            fast -= Unity.
        ].

        Transcript log cr.
    ]
]
