@Notice :'Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for more details.'!

Hoot Streams importAll.

Object subclass: StreamsTest. "Confirm proper operation of Hoot Streams."

StreamsTest members:
[
    @Static! Void! main: Java Lang String! ... args [ StreamsTest basicNew testStreams. ]

    testStreams [
        fileName := 'target/stream.test.txt'.
        fileOut := 'target/parsed.text.txt'.
        text := 'a quick brown fox jumped over a lazy dog'.

        "Test file streams."
        oStream := TextFileWriteStream write: fileName.
        oStream nextPutAll: text; cr.
        oStream close.

        Transcript log print: 'wrote into '; print: fileName; print: ': '; print: text; cr.
        Transcript log print: 'read  from '; print: fileName; print: ': '.

        iStream := TextFileReadStream read: fileName.
        newStream := TextFileWriteStream write: fileOut.
        [ iStream atEnd ] whileFalse: [
            String! line := iStream upTo: $  .
            newStream nextPutAll: line; cr.
            Transcript log print: line; cr.
        ].
        iStream close.
        oStream close.

        "Test collection streams."
        vowels := 'aeiou'.
        source := 'abcdefghijklmnopqrstuvwxyz'.
        tiStream := source readStream.
        TextWriteStream! toStream := String type writeStream: source size.
        [ tiStream atEnd ] whileFalse: [
            c := tiStream next.
            (vowels includes: c) ifFalse: [ toStream nextPut: c ].
        ].
        Transcript log
            print: source; print: ' w/o vowels = ';
            printLine: toStream contents; cr.
    ]
]
