### Hoot ###

![Hoot Owl][logo]

Hoot is a variation of [Smalltalk][smalltalk] that integrates the best features of Smalltalk
with those of a host language and its VM, such as [Java][java] (and the [JVM][jvm]) or [C#][csharp] (and the [CLR][clr]).
Hoot is an experimental new programming language which builds on experience and understanding gained during
the development of [Bistro][bistro].

Hoot extends some of the ideas pioneered in Bistro:

* the [simplicity][st-syntax] and expressiveness of Smalltalk,
* support for _some_ standard Smalltalk type protocols,
* support for _optional_ type annotations,
* easy integration with other class libraries (Hoot or otherwise).

Whereas, the Bistro grammar included and supported some Java keywords like `static`, `public`, `private`,
Hoot makes all such code decorations optional annotations, and it uses naming conventions, type inference,
and Lambda functions to support strong dynamic types at runtime.

Hoot does not provide its own [virtual machine][st-image], like some [Smalltalks][st-imps].
Rather, Hoot takes advantage of already existing VMs that have matured over the past few decades.
Also, Hoot does not use [image-based persistence][st-image] for storing its code.
Rather, like many programming languages, it uses simple text files.
This allows developers to use popular tools for text editing and source code [version control][version-control], 
just as this [Git][git-doc] repository contains the code for Hoot itself.

#### Platform Requirements ####

Hoot was originally developed with Java SE 8 [JDK 8][jdk8], partly due to its [Long Term Support][java-lts] LTS
and support for [Lambdas][lambdas].
However, Java SE 11 has since become available, also with LTS and a few nice language enhancements, including
better type inference and support for local [var][inference] declarations.

Hoot has now adopted these capabilities.
These advances in the JDK allow the Hoot compiler to generate simpler Java code.
So, Hoot now requires at least Java SE 11 [JDK][jdk11].
You'll also need [Maven][maven], at least [version 3.5.0][maven-350] is recommended.

#### Building from Sources ####

Clone this repository, and run the following shell command in the base folder:

```
mvn -U -B clean install
```

This will build all the Hoot compiler and runtime components, then generate Java sources from the Hoot sources
for the various Hoot library types and classes, and then run the tests of the samples.
This also resembles how the associated [build pipeline][build] runs in [Docker][maven-docker].

Alternatively, you can build and run the samples as follows:

```
mvn -U -B -pl libs-samples -am clean install
```

As indicated above, the Maven projects in this repo have a dependency structure that helps build and run the samples.
Their transitive dependencies are structured such that upstream libraries get built before the downstream libraries 
which need those upstream.
Each Maven project in this repo has some test fixture(s).
These also help order the overall build process.

Some of the Maven projects have no **main** Java sources, only Hoot sources and Java **test** code.
In these cases, their's a test fixture that runs the [Hoot compiler][usage] to generate the Java code 
for the associated Hoot sources.

| **Project** | **Contents** |
| ----------- | ------------ |
| code-smalltalk | Hoot sources for the Smalltalk library types + **CompilerTest** |
| libs-smalltalk | placeholders + _generated_ Java sources from _code-smalltalk_ |
| code-hoot | Hoot sources for the Hoot class library + **CompilerTest** |
| libs-hoot | placeholders + _generated_ Java sources from _code-hoot_ |
| code-samples | Hoot sources for the examples + **ExamplesTest** |
| libs-samples | _another_ **ExamplesTest** + _generated_ Java sources from _code-samples_ |

While the [Hoot compiler commands][usage] offer several options, it provides some defaults to simplify its use.
This allows the test fixtures mentioned above to mimic what might otherwise be done with a compiler command.
Here's an example from test fixture **ExamplesTest**.

```java
public class ExamplesTest {
    
    static final String LibsSamples = "../libs-samples";
    static final String[] SamplePackages = { "Hoot.Examples", };
    @Test public void compileLibrary() { main(buildCleanCommand(LibsSamples, SamplePackages)); }

} // ExamplesTest
```

In this example, the Hoot source code from **Hoot.Examples** in the _code-samples_ project is being translated 
into Java code and placed into the proper Maven folder structure in the _libs-samples_ project.
This pairing and interleaving of the Maven projects is what allows the proper processing of the Hoot and generated Java
libraries, so that all the parts get built in their proper order.
Finally, there's another (different) **ExamplesTest** contained in _libs-samples_ that runs all the sample tests whose Java code was 
generated from the _code-samples_ project.

#### Repository Contents ####

This repository contains the Hoot compiler, and its runtime, type and class libraries, and design docs.
Overall, the Hoot compiler performs source-to-source translation (transcoding) from Hoot to a host language.
Then, the Hoot compiler uses a standard host language compiler to translate the intermediate sources
into class files native to the host language runtime.

Most of these discussions reference [Java][java] and how the Hoot compiler translates Hoot into Java.
However, these discussions also generally apply to [C#][csharp] (on the [CLR][clr]), especially as regards
how Hoot maps the Smalltalk [object model][diagram] into a host language and its platform.

The Hoot compiler was built with [ANTLR 4][antlr], and [StringTemplate][st].
This project uses [ANTLR][antlr] to generate the Hoot parser from [its grammar][grammar].
The Hoot compiler accepts directions with a command line interface CLI, that it uses to invoke the parser.
The parser recognizes the code in Hoot source files, and builds trees of ST instances using
the [StringTemplate][st] library.
The ST instances then use [code generation templates][code-lib] to output the corresponding host language source code.

The repository is organized into several Maven projects, each of which builds a portion of Hoot overall.

* [Hoot Abstractions](hoot-abstracts#hoot-abstractions)
* [Hoot Runtime Classes](hoot-runtime#hoot-runtime-library)
* [Hoot Compiler Classes](hoot-compiler#hoot-compiler-library)
* [Smalltalk Protocol Type Library](code-smalltalk#smalltalk-type-library)
* [Hoot Class Library](code-hoot#hoot-class-library)
* [Hoot Sample Code + Tests](code-samples#hoot-examples)

#### Features ####

Many of the following features were originally developed in the context of [Bistro][bistro].
However, Hoot provides several improvements over Bistro.
For example, Hoot has a uniform model for the annotations found in supported host languages
and for the various language-specific code decorations, e.g., `static`, `public`, `private`, etc.
Each link below leads to discussions of the specific feature design details.

| **Feature** | **Description** |
| ----------- | --------------- |
| [Language Model][model]    | Hoot has a _declarative_ language model. |
| [Name Spaces][spaces]      | Hoot class packages and name spaces are assigned based on folders. |
| [Classes][classes]         | Hoot classes have a hybrid structure based primarily on Smalltalk. |
| [Metaclasses][classes]     | Hoot supports meta-classes like those found in Smalltalk. |
| [Types][types]             | Hoot supports first-class interfaces (as types). |
| [Metatypes][types]         | Hoot types can have associated meta-types. |
| [Access Control][access]   | Hoot code may use access controls: @**Public**, @**Protected**, @**Private**. |
| [Decorations][decor]       | Hoot also supports: @**Abstract**, @**Final**,   @**Static**. |
| [Annotations][notes]       | Hoot translates other annotations into those of its host language. |
| [Optional Types][optional] | Hoot variable and argument type specifications are optional. |
| [Generic Types][generics]  | Hoot supports definition and use of generic types. |
| [Methods][methods]         | Hoot methods resemble those of Smalltalk. |
| [Interoperability][xop]    | Hoot method names become compatible host method names. |
| [Primitives][prims]        | Hoot supports @**Primitive** methods. |
| [Comments][comments]       | Hoot comments are copied into the host language. |
| [Standard Library][lib]    | Hoot includes types that define [ANSI Smalltalk][st-ansi] protocols. |
| [Blocks][blocks]           | Hoot blocks are implemented with Java Lambdas. |
| [Threads][threads]         | Hoot blocks support the **fork** protocol for spawning threads. |
| [Exceptions][except]       | Hoot supports both Smalltalk and Java exception handling. |
| [Questions][faq]           | Frequently Asked Questions - FAQ about Hoot |

```
Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
```
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for LICENSE details.


[bistro]: https://bitbucket.org/nik_boyd/bistro-smalltalk/ "Bistro"
[smalltalk]: https://en.wikipedia.org/wiki/Smalltalk "Smalltalk"
[st-syntax]: https://en.wikipedia.org/wiki/Smalltalk#Syntax "Smalltalk Syntax"
[st-imps]: https://en.wikipedia.org/wiki/Smalltalk#List_of_implementations "Smalltalk Implementations"

[jdk8]: https://openjdk.java.net/projects/jdk8/
[jdk11]: https://openjdk.java.net/projects/jdk/11/
[java-lts]: https://www.oracle.com/technetwork/java/java-se-support-roadmap.html
[java]: https://en.wikipedia.org/wiki/Java_%28programming_language%29 "Java"
[jvm]: https://en.wikipedia.org/wiki/Java_virtual_machine "Java Virtual Machine"
[lambdas]: https://www.oracle.com/webfolder/technetwork/tutorials/obe/java/Lambda-QuickStart/index.html
[inference]: https://developer.oracle.com/java/jdk-10-local-variable-type-inference

[csharp]: https://en.wikipedia.org/wiki/C_Sharp_%28programming_language%29 "C#"
[clr]: https://en.wikipedia.org/wiki/Common_Language_Runtime "Common Language Runtime"
[st]: https://www.stringtemplate.org/ "StringTemplate"
[antlr]: https://www.antlr.org/ "ANTLR"
[maven]: https://maven.apache.org/
[maven-350]: https://maven.apache.org/docs/3.5.0/release-notes.html
[maven-docker]: https://hub.docker.com/_/maven/

[logo]: https://gitlab.com/hoot-smalltalk/hoot-smalltalk/raw/master/hoot-design/hoot-owl.svg "Hoot Owl"

[hoot-ansi]: hoot-design/ANSI-X3J20-1.9.pdf
[squeak-ansi]: http://wiki.squeak.org/squeak/172
[st-ansi]: https://web.archive.org/web/20060216073334/http://www.smalltalk.org/versions/ANSIStandardSmalltalk.html
[st-image]: https://en.wikipedia.org/wiki/Smalltalk#Image-based_persistence
[version-control]: https://en.wikipedia.org/wiki/Version_control#Overview
[git-doc]: https://git-scm.com/

[grammar]: hoot-compiler/src/main/antlr4/Hoot/Compiler/Hoot.g4
[code-lib]: hoot-compiler/src/main/resources/CodeTemplates.stg

[build]: .gitlab-ci.yml#L24
[model]: hoot-design/model.md#language-model "Language Model"
[spaces]: hoot-design/libs.md#name-spaces "Name Spaces"
[classes]: hoot-design/libs.md#classes-and-metaclasses "Classes"
[types]: hoot-design/libs.md#types-and-metatypes "Types"
[access]: hoot-design/notes.md#access-controls "Access Controls"
[notes]: hoot-design/notes.md#annotations "Annotations"
[decor]: hoot-design/notes.md#decorations "Decorations"
[optional]: hoot-design/notes.md#optional-types "Optional Types"
[generics]: hoot-design/notes.md#generic-types "Generics"
[methods]: hoot-design/methods.md#methods "Methods"
[comments]: hoot-design/methods.md#comments "Comments"
[xop]: hoot-design/methods.md#interoperability "Interoperability"
[prims]: hoot-design/methods.md#primitive-methods "Primitives"
[lib]: code-smalltalk#smalltalk-type-library "Library"
[blocks]: hoot-design/blocks.md#blocks "Blocks"
[except]: hoot-design/exceptions.md#exceptions "Exceptions"
[faq]: hoot-design/faq.md#frequently-asked-questions "Questions"
[usage]: hoot-design/usage.md#hoot-compiler-usage "Usage"
[threads]: hoot-design/blocks.md#threads
