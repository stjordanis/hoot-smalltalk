#### Hoot Compiler Usage ####

The Hoot compiler has a command line interface CLI.
Running `hoot-compiler --help` produces the following:

```
usage: hoot-compiler
 -b,--base <basePath>           base folder path
 -c,--clean                     removes any previously generated code
 -f,--folder <targetPath>       target Java folder path
 -h,--help                      displays this help
 -p,--packages <packageNames>   packages to compile
 -s,--source <sourcePath>       Hoot sources folder path
```

| **Argument** | **Description** | **Default** |
| ------------ | --------------- | ----------- |
| basePath | overall base folder | the current working directory (from which command is run) |
| sourcePath | the base folder relative to which Hoot packages will be read | **basePath**/src/main |
| targetPath | the target folder relative to which Java code will be written | **basePath**/src/main/java |
| packageNames | names of the packages under **sourcePath** to be compiled | all packages under **sourcePath** |

#### Notes

The Hoot compiler is a trans-coding compiler. 
It presently converts Hoot source code into Java source code, which then itself gets compiled by a Java compiler.

However, it presumes a tooling context: [Maven][maven], and folders structured according to Maven's organization conventions.
Conforming to these conventions, the Hoot compiler locates its sources and Java target folders conveniently for Maven.
However, the defaults can be overridden.

Some care should be taken when overriding the **targetPath** and using the `clean` option.
The `clean` option will remove _**all**_ folders and files under the **targetPath**.
So, use this carefully. 
Before using the `clean` option, be sure your **targetPath** points to a folder intended to contain the generated Java code.


```
Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
```
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for LICENSE details.


[maven]: https://maven.apache.org/
