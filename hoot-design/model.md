#### Language Model ####

Traditionally, [Smalltalk][smalltalk] systems were built in the context of an object memory [image][images].
Smalltalk class definitions loaded from a source file were _interpreted_ in the context of
the memory [image][images].
While an [image][images]-based development environment contributes significantly to the agility of Smalltalk's
integrated suite of tools, it introduces some difficulties for source code management, component management,
system configuration, and release management in large development projects.

In contrast, many other programming languages are text file based for their source code,
including both [Java][java] and [C#][csharp].
Such declarative, file based programming languages make it much easier to use popular tools for version
control (like [Git][git] and [GitHub][github]) and component management (like [Nexus][nexus]).
For example, the [Java][java] language is file based, both for its source code and its executable binaries.
Also, the [Java][java] platform uses archive files (JARs, WARs, ...) to deploy its compiled class libraries.
The [C#][csharp] language is also file based for its source code, and its compiled binaries are packaged
as dynamic link library (DLL) files.

Thus, in order to better support source code management, better correspond with its target languages, and
thereby ease the utilization of those platforms, Hoot uses a text based, _declarative language model_
for its source code files (classes and types).
Here's an abbreviated example of a Hoot source code file (for a Point class):

```
"Hoot Geometry Point"
Hoot Magnitudes Magnitude import.

Magnitude subclass: Point. "A point on a 2-dimensional plane."
Point class "creating instances" members: [ "..." ].
Point "point arithmetic" members: [ "..." ].
```

#### Hoot Class Structure ####

The overall source code structure of each Hoot class resembles a series of Smalltalk messages.
Note that the Point class is located in the **Hoot Geometry** package, which the Hoot compiler assigns,
as it translates the Point class into Java.
Notice the example above shows an `import` for the Magnitude class.
The Hoot compiler generates some of the more common imports of runtime classes as it translates Hoot code into its
target language. See the discussion [Name Spaces](libs.md#name-spaces) for more about packages and imports.

The following table provides a more complete set of examples of possible Hoot class and type definition
templates.


| **Class Definitions** | **Type Definitions** | **Type Implementation** |
| --------------------- | -------------------- | ----------------------- |
| nil `subclass:` RootClass.        | nil `subtype:` RootType.       | nil `subclass:` TypeName! RootClass.        |
| Superclass `subclass:` Subclass.  | Supertype `subtype:` Subtype.  | Superclass `subclass:` TypeName! Subclass.  |


Notice that root classes and types are derived from **nil**.
Root types have no equivalent Java supertype, but root classes are derived from **java lang Object**
and root meta-classes are derived from **Hoot Behaviors Class**.
See the discussion about [Metaclasses](#classes-and-metaclasses) for more details.

Hoot classes implement types with the same syntax used to specify the type of a variable:
the type name followed by an exclamation point **!**.
So, a Hoot class implements multiple types simply by mentioning them in a sequence before the subclass name.

```
Hoot Exceptions ExceptionBuilder import.
Hoot Exceptions SignaledException import.
Object subclass: ExceptionBuilder! SignaledException! Exception.
```

A Hoot type definition works a little differently. Each type can extend multiple super-types, but using
a comma-separated list.

```
ExceptionDescription, ExceptionSignaler subtype: ExceptionBuilder.
```

```
Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
```
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for LICENSE details.

[smalltalk]: https://en.wikipedia.org/wiki/Smalltalk "Smalltalk"
[images]: https://en.wikipedia.org/wiki/Smalltalk#Image-based_persistence "Image Persistence"
[java]: https://en.wikipedia.org/wiki/Java_%28programming_language%29 "Java"
[csharp]: https://en.wikipedia.org/wiki/C_Sharp_%28programming_language%29 "C#"
[antlr]: https://www.antlr.org/ "ANTLR"
[st]: https://www.stringtemplate.org/ "StringTemplate"
[git]: https://git-scm.com/ "Git"
[github]: https://github.com/ "GitHub"
[gitlab]: https://gitlab.com/ "GitLab"
[nexus]: https://www.sonatype.com/nexus "Sonatype Nexus"
[generics]: https://en.wikipedia.org/wiki/Parametric_polymorphism "Generic Types"
