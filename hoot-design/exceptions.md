#### Exceptions ####

While some superficial similarities exist between [Smalltalk][smalltalk] and [Java][java] exception
handling mechanisms, there are also some fundamental differences.
So, deciding whether and how to integrate these mechanisms presented one of the more challenging
problems for the design of [Bistro][bistro].
Hoot benefits from the understanding of those experiments.
The following table compares the salient aspects of the Smalltalk and Java exception handling mechanisms.

| **Language** | **Feature** |
| ------------ | ----------- |
| Java      | models exceptions as instances of exception classes (**Throwable** and its subclasses) |
| Smalltalk | models exceptions as instances of exception classes (**Exception** and its subclasses) |
| Java      | special syntax for dealing with exceptions: `try { } catch { } finally { }`, `throw`, and `throws` |
| Smalltalk | no special syntax, only standard message idioms `[ ] catch: [ ] ensure: [ ]`, `[ ] ifCurtailed: [ ]` |
| Java      | exception handling is strictly stack oriented - once unwound, old stack frames are unavailable |
| Smalltalk | fine grained control: whether + when stack frames are unwound, whether execution resumes at the point of origin |
| Java      | method signatures must declare checked exceptions with a `throws` clause |
| Smalltalk | exceptions never impact method signatures |

```
Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
```
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for LICENSE details.


[bistro]: https://bitbucket.org/nik_boyd/bistro-smalltalk/ "Bistro"
[smalltalk]: https://en.wikipedia.org/wiki/Smalltalk "Smalltalk"
[images]: https://en.wikipedia.org/wiki/Smalltalk#Image-based_persistence "Image Persistence"
[java]: https://en.wikipedia.org/wiki/Java_%28programming_language%29 "Java"
[csharp]: https://en.wikipedia.org/wiki/C_Sharp_%28programming_language%29 "C#"
[antlr]: https://www.antlr.org/ "ANTLR"
[st]: https://www.stringtemplate.org/ "StringTemplate"
[git]: https://git-scm.com/ "Git"
[github]: https://github.com/ "GitHub"
[nexus]: https://www.sonatype.com/nexus "Sonatype Nexus"
[generics]: https://en.wikipedia.org/wiki/Parametric_polymorphism "Generic Types"
