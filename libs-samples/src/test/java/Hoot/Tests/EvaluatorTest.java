package Hoot.Tests;

import Hoot.Compiler.Scopes.ClosureCompiler;
import Hoot.Magnitudes.SmallInteger;
import Hoot.Runtime.Faces.Logging;
import org.codehaus.janino.ExpressionEvaluator;
import org.codehaus.janino.ScriptEvaluator;
import org.junit.*;

/**
 * Confirms proper operation of the closure compiler.
 * @author nik <nikboyd@sonic.net>
 */
public class EvaluatorTest implements Logging {
    
    static final Object[] NoArgs = {};
    static final String EvalEquals = "%s = %s";

    @Ignore
    @Test public void evalMath() throws Exception {
        var test = "3 + 4";
        ExpressionEvaluator ee = new ExpressionEvaluator(); ee.cook(test);
        report(format(EvalEquals, test, ee.evaluate(NoArgs).toString()));
    }
    
    @Test public void scriptTest() throws Exception {
        var test = "{ int a = 3; int b = 4; return a + b; }";
        ScriptEvaluator s = new ScriptEvaluator(test, int.class);
        report(format(EvalEquals, test, s.evaluate(NoArgs).toString()));
    }
    
    @Test public void blockTest() throws Exception {
        var test = "[ 3 + 4 ]";
        ClosureCompiler c = new ClosureCompiler();
        c.parseClosure(test);
        SmallInteger s = (SmallInteger)c.evaluate();
        report(format(EvalEquals, test, s.printString().toString()));
    }

} // EvaluatorTest
