package Hoot.Runtime;

import java.util.*;
import org.junit.*;

import Hoot.Runtime.Maps.Package;
import Hoot.Runtime.Maps.ClassPath;
import Hoot.Runtime.Behaviors.Mirror;
import Hoot.Runtime.Behaviors.Typified;
import static Hoot.Runtime.Functions.Utils.*;
import static Hoot.Runtime.Behaviors.Typified.*;
import static Hoot.Runtime.Maps.Library.CurrentLib;
import Hoot.Runtime.Faces.Logging;

/**
 * Confirms proper operation of path loading.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
//@Ignore
public class PathTest implements Logging {

    public static class Submirror extends Mirror {
        public Submirror() {
            this(Submirror.class);
        }

        public Submirror(Class<?> reflectedClass) {
            super(reflectedClass);
        }

        @Override public boolean isReflective() {
            return false;
        }
    }

    private static final String SourceFolder = "../code-smalltalk/src/main/Smalltalk";
    private static final String TargetFolder = "../hoot-abstracts/target/classes";
    private static final String ClassFolder  = "../hoot-runtime/target/classes";
    private static final String[] BasePaths = { SourceFolder, TargetFolder, ClassFolder, };

    @BeforeClass
    public static void prepare() {
        ClassPath.loadBasePaths(BasePaths);
    }

    @Test
    public void loadPaths() {
        int faceCount = CurrentLib.countFaces();
        int pkgsCount = CurrentLib.countPackages();
        int pkgdCount = CurrentLib.countPackagedFaces();
        report(format(Report, pkgsCount, faceCount, pkgdCount));
//        assertTrue(faceCount == pkgdCount);

        String testPackage = getClass().getPackage().getName();
        Set<String> packages = CurrentLib.packageNames();
        packages.stream().filter(p -> p.startsWith("Hoot.")).forEach(n -> {
            Package p = Package.named(n);
            if (!p.fullName().equals(testPackage)) {
                int count = p.countFaces();
                report(p.fullName() + " has " + count + " faces");
            }
        });

        Package test = Package.named(testPackage);
        report("");
        reflectOn(test);

        // confirm that a Mirror can be replaced
        test.addFace(new Submirror());
        report("");
        report("after replacing Submirror,");
        reflectOn(test);

        // enumerate types in Emissions
        String[] packageNames = {
            "Hoot.Runtime.Emissions",
            "Hoot.Runtime.Maps",
            "Hoot.Runtime.Names",
        };
        wrap(packageNames).forEach(name -> {
            Package p = Package.named(name);
            if (p == null) {
                report("can't locate " + name);
            }
            else {
                report(p);
            }
        });
    }

    private void reflectOn(Package p) {
        report(p.fullName() + " has:");
        p.packagedClassNames().forEach(n -> {
            Typified type = p.faceNamed(n);
            report(format(Reflection, type.name(), type.isReflective()+""));
        });
    }

    private void report(Package p) {
        report("");
        report(p.fullName());
        p.packagedClassNames().forEach(n -> {
            Typified type = p.faceNamed(n);
            if (type == null) {
                report("can't locate " + n + " in " + p.fullName());
            }
            else {
                List<Typified> imps = type.typeHeritage();
                List<Typified> heritage = type.simpleHeritage();
                report(format(Classify, type.name()));
                if (!heritage.isEmpty()) {
                    report(format(Extends, names(heritage).toString()));
                }
                if (!imps.isEmpty()) {
                    report(format(Imps, names(imps).toString()));
                }
            }
        });
    }

    static final String Report = "pkgs: %d, faces: %d, pkgd: %d";
    static final String Reflection = "  %s reflects: %s";
    static final String Classify = "  %s";
    static final String Extends = "    extends %s";
    static final String Imps = "    implements %s";
    static final String Empty = "";
}
