package Hoot.Runtime;

import Hoot.Runtime.Blocks.Enclosure;
import java.util.*;
import org.junit.Test;
import static org.junit.Assert.*;

import Hoot.Runtime.Faces.*;
import Hoot.Runtime.Behaviors.*;
import Hoot.Runtime.Names.Selector;
import Hoot.Runtime.Values.Frame;
import Hoot.Runtime.Values.Value;
import static Hoot.Runtime.Functions.Utils.*;

/**
 * Verifies proper operation of stack frames and named values.
 * @author Copyright 2010,2017 Nikolas S. Boyd. All rights reserved.
 * @author nik <nikboyd@sonic.net>
 */
public class ValueTest implements Logging {

    private static final String More = "more";
    private static final String Less = "less";

    @Test
    public void sampleNumber() {
        Integer x = 5;
    }

    @Test
    public void sampleList() {
        String[] listA = { "a", "b", "c", "d", "e", };
        String[] listB = { "a", "b", "c", "d", "e", };
        String[] listC = { "A", "B", "C", "D", "E", };
        String[] listD = { "A", "B", "C", };

        assertTrue(wrap(listA).equals(wrap(listA)));
        assertTrue(wrap(listA).equals(wrap(listB)));
        assertFalse(wrap(listA).equals(wrap(listC)));
        assertFalse(wrap(listA).equals(wrap(listD)));
    }

    @Test
    public void sampleFrame() {
        Frame f = new Frame();
        f.bind("sample", 5);
        report(f.getValue("sample").toString());

        f.with(Value.named("example"));
        report(f.getValue("example").toString());

        f.bind("example", 5);
        Value<Integer> v5 = f.getValue("example");
        report(v5.toString());
        Integer five = v5.value();

        Value v = f.getValue("example");
        report(v.toString());
        five = Value.as(Integer.class, v);
        Value<Integer> vi = v.asType(Integer.class);
        five = vi.value();

        f.bind("example", "test");
        report(f.getValue("example").toString());
        report(f.toString());

        {
            Frame f0 = new Frame();
            f0.push(5);
            ((Integer)f0.topValue()).toString();
        }
    }

    @Test
    public void sampleArray() throws Throwable {
        String[] sample = { "a", "b" };
        Value v = Value.with(sample);
        report(v.toString());
    }

    @Test
    public void sampleStatic() throws Throwable {
        Selector c = Selector.named("Hoot.Runtime.Names.Primitive");
        Selector m = Selector.named("elementaryTrue");
        Boolean result = Invoke.withValues(c.toClass()).with(m).call(Boolean.class);
        assertFalse(result == null);
        report(result.getClass().getSimpleName() + ": " + result);
    }

    @Test
    public void sampleInstance() throws Throwable {
        Selector c = Selector.named("Hoot.Runtime.Values.Frame");
        Class<?> type = c.toClass();
        assertFalse(type == null);

        Object result = Invoke.with().with(c).call();
        assertFalse(result == null);
        report(result.getClass().getSimpleName());
    }

    @Test
    public void samplePerform() throws Throwable {
        Object result =
            Invoke.with(Value.self(this), Value.with('5'), Value.with(5))
                .with(Selector.named("sampleCall"))
                .call();

        assertFalse(result == null);
        report(result.getClass().getSimpleName());
    }

    @Test
    public void sampleExit() {
        final String exitID = "sampleExit";
        Valued result = new Frame(exitID).evaluate(f0 -> {
            samplePassThrough(Enclosure.withBlock(f1 -> {
                return f1.exit(exitID, Selector.named("awesome!"));
            }));
        });

        report(result.toString());
    }

    public Valued samplePassThrough(Enclosure c) {
        final String exitID = "samplePassThrough";
        return new Frame(exitID).evaluate(f0 -> {
            report("before call");
            Enclosure.withBlock(f1 -> { c.value(); }).value();
            report("after call");
            assertFalse("early closure return failed!", true);
        });
    }

    @Test
    public void sampleBlockZeroArgument() {
        Enclosure a = Enclosure.withBlock(f -> {
            report("honorable mention");
        });

        a.value();

        Enclosure c = Enclosure.withBlock(f -> {
            return Selector.named("awesome!");
        });
        List<Selector> results = wrap((Selector)c.value());
        report("count: " + results.size());
        for (Selector s : results) {
            report(s.toString());
        }

        Selector[] unwrapped = { };
        unwrapped = unwrap(results, unwrapped);
        assertFalse(unwrapped == null);
        assertTrue(unwrapped.length == results.size());
    }

    @Test
    public void sampleBlockOneArgument() {
        String[] samples = { "sample", "text" };
        Enclosure c = Enclosure.withBlock(f -> {
            String s = f.getValue(0).name();
            return Selector.named(s);
        });
        List<Valued> results = (List<Valued>)c.evaluateWithEach(samples);
        report("count: " + results.size());
        for (Valued s : results) {
            report(s.toString());
        }
    }

    @Test
    public void sampleBlockTwoArguments() throws Throwable {
        Frame frame = Frame.withValues(Value.named(More, 5), Value.named(Less, 2));

        IntegerValue r = frame.evaluate(Enclosure.withBlock(f -> {
            Integer less = f.getValue(Less).value();
            Integer more = f.getValue(More).value();
            return IntegerValue.Source.with(less + more);
        }));
        report("eval = " + r);
    }

    @Test
//    @Ignore
    public void sampleBlockFromBehavior() {
        HashSet<Typified> result = new HashSet();

        this.allSubclassesDo(Enclosure.withBlock(f -> {
            Typified each = f.getValue(0).value();
            result.add(each);
        }));

        report("eval = " + result.size());
    }

    private Object allSubclassesDo(Enclosure c) {
        c.evaluateWithEach(Mirror.forClass(getClass()));
        return this;
    }

    @Test
    public void sampleMethodCall() throws Throwable {
        Selector s = Selector.named("sampleCall");
        Frame f = new Frame();
        f.push(Value.self(this));
        f.push(Value.with('5'));
        f.push(Value.with(5));
        Object result = f.perform(s);
        assertFalse(result == null);
    }

    @Test
    public void sampleStack() {
        Frame f = new Frame();
        f.push(5);
        f.push('5');
        report(Empty + f.pop());
        report(Empty + f.pop());

        f.push(5);
        f.push('5');
        sampleCall((Character)f.popValue(), (Integer)f.popValue());

        f.push(5);
        f.push('5');
        Value<?>[] vs = f.popReversed(2);
        sampleCall((Integer)vs[0].value(), (Character)vs[1].value());

        // x + y => push y, push x, pop x.plus( pop y )
        // x + y => ops.push(x), msgs.push(+), ops.push(y), s.push(ops.pop()), s.push(ops.pop()), s.pop().plus(s.pop())
    }

    public Object sampleCall(Character c, Integer x) {
        report(c + " = " + x);
        Integer result = x + c;
        return result;
    }

    private void sampleCall(Integer x, Character c) {
        report(c + " = " + x);
    }

} // ValueTest
