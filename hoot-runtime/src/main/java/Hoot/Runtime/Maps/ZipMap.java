package Hoot.Runtime.Maps;

import java.io.*;
import java.util.*;
import java.util.zip.*;

import static Hoot.Runtime.Maps.Package.*;
import static Hoot.Runtime.Maps.Library.*;
import static Hoot.Runtime.Functions.Utils.*;

/**
 * Maps the classes located in an element of a class path.
 * Locates all package directories relative to an archive from the class path.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 1999,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class ZipMap extends PathMap {

    public ZipMap(String pathname) { super(pathname); }

    /**
     * @return null - file operations are not supported in archives.
     * @param folderName the name of a Java/Hoot package directory.
     */
    @Override public File locate(String folderName) { return (null); }

    /**
     * @return whether the supplied pathname identifies a support file type.
     * @param pathname the pathname of a potential archive file.
     */
    public static boolean supports(String pathname) {
        return matchAny(FileTypes, type -> pathname.endsWith(type)); }

    /**
     * Loads a class name from an archive entry if it identifies a class file.
     * @param entry an archive file entry.
     */
    protected void load(ZipEntry entry) {
        String name = entry.getName();
        int i = name.lastIndexOf(Slash);
        String path = (i < 0 ? Empty : name.substring(0, i));
        String file = name.substring(i + 1);

        List<String> classNames = map.get(path);
        if (classNames == null) {
            classNames = new ArrayList();
            map.put(path, classNames);
        }

        if (file.endsWith(ClassFileType)) {
            int length = file.length() - ClassFileType.length();
            classNames.add(file.substring(0, length));
        }
    }

    /**
     * Loads the names of all the packaged classes located in the archive.
     */
    @Override public void load() {
        try (ZipFile zipFile = new ZipFile(basePath)) {
            Enumeration e = zipFile.entries();
            while (e.hasMoreElements()) {
                load((ZipEntry) e.nextElement());
            }
        }
        catch (IOException ex) {
            error(ex.getMessage(), ex);
        }
    }

    public static final String[] SupportedFileTypes = { ".zip", ".ZIP", ".jar", ".JAR" };
    public static final List<String> FileTypes = wrap(SupportedFileTypes);

} // ZipMap
