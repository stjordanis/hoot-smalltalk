package Hoot.Runtime.Maps;

import java.io.*;
import java.util.*;

import Hoot.Runtime.Names.Name;
import Hoot.Runtime.Faces.Named;
import Hoot.Runtime.Names.TypeName;
import Hoot.Runtime.Behaviors.Mirror;
import Hoot.Runtime.Behaviors.Typified;
import static Hoot.Runtime.Functions.Utils.*;
import static Hoot.Runtime.Names.Operator.*;
import static Hoot.Runtime.Maps.ClassPath.*;
import static Hoot.Runtime.Names.Primitive.*;

/**
 * Maintains references to all classes and interfaces imported from external packages.
 * Packages are located relative to the system class path established by the Java environment.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 1999,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class Library implements TypeName.Resolver {

    protected Library() { }
    public static final Library CurrentLib = new Library();
    public File locate(String folder) { return CurrentPath.locate(folder); }
    public boolean canLocate(Package aPackage) { return CurrentPath.canLocatePackage(aPackage); }
    public static Typified findFace(String faceName) { return CurrentLib.faceNamed(faceName); }

    Map<String, Typified> faces = new HashMap();
    public int countFaces() { return distinctFaces().size(); }
    public void removeFace(String faceName) { faces.remove(faceName); }
    public Typified faceFrom(Named reference) { return faceNamed(reference.fullName()); }
    public Set<String> distinctFaces() { return selectSet(faces.keySet(), f -> f.contains(Dot)); }
    public Typified faceNamed(String fullName) {
        if (hasNo(fullName)) return Mirror.emptyMirror();
        String faceName = Name.typeName(fullName);
        String packageName = Name.packageName(fullName);
        return packageName.isEmpty() ? faces.get(faceName) :
                packageNamed(packageName).faceNamed(faceName); }

    Map<String, Package> packages = new HashMap();
    protected Map<String, Package> packages() { return this.packages; }
    protected Collection<Package> allPackages() { return packages().values(); }
    public int countPackages() { return packages().size(); }
    public Set<String> packageNames() { return packages().keySet(); }
    void clear() { faces.clear(); packages().clear(); }
    public Package packageNamed(String packageName) {
        if (packages().containsKey(packageName)) {
            return packages().get(packageName);
        }

        Package result = new Package(packageName);
        packages().put(packageName, result);
        return result;
    }

    File sourceBase;
    public String sourcePath() { return sourceBase.getAbsolutePath(); }

    File targetBase;
    public String targetPath() { return targetBase.getAbsolutePath(); }

    File classBase;
    public String classPath() { return classBase.getAbsolutePath(); }
    public String[] basePaths() {
        String[] results = { sourcePath(), targetPath(), classPath() }; return results; }

    void configurePaths(List<File> basePaths) {
        clear();
        sourceBase = basePaths.get(0);
        targetBase = basePaths.get(1);
        classBase  = basePaths.get(2);
    }

    /**
     * The names of those classes shared in common by both Hoot and Java.
     */
    private static final String[] CommonClasses = { 
        "Object", "Boolean", "Character", "String", "Number", "Double", 
        "Float", "Integer", "Class", "Exception", "Error", "Array", 
    };
    public void removeShadowedClasses() { wrap(CommonClasses).forEach(c -> removeFace(c)); }

    public static final String[] RootPackages = { "java.lang", "java.lang.reflect", };
    public void loadBasePackages() { wrap(RootPackages).forEach(p -> packageNamed(p).loadFaces()); }
    public void loadEmptyPackages() { selectSet(allPackages(), p -> p.needsLoad()).forEach(p -> p.loadFaces()); }
    public int countPackagedFaces() { return reduce(map(allPackages(), p -> p.countFaces()), Integer::sum, 0); }

    /**
     * @param face a typed face
     */
    public void addFace(Typified face) {
        String fullName = face.fullName();
        String typeName = Name.typeName(fullName);
        String packageName = Name.packageName(fullName);
        faces.put(fullName, face);
        faces.put(typeName, face);

        if (!packageName.isEmpty() && typeName.startsWith(packageName)) {
            typeName = typeName.substring(packageName.length() + 1);
            faces.put(typeName, face);
        }

        if (Name.isMetaNamed(fullName)) {
            faces.put(fullName.replace(Dollar, Dot), face);
            faces.put(typeName.replace(Dollar, Dot), face);
        }

        whisper("added " + fullName + " to Library");
    }


    public Class resolveType(Named reference) { return nullOr(f -> f.primitiveClass(), faceFrom(reference)); }
    @Override public TypeName resolveTypeNamed(Named reference) { return TypeName.fromOther(faceFrom(reference)); }

    /**
     * @return whether the container can resolve a named reference.
     * @param reference a symbolic reference to be resolved.
     */
//    public boolean resolves(Named reference) {
//        if (reference.name().toString().isEmpty()) return false;
//        if (reference.name().equals(Primitive)) return true;
////        if (reference.isElementary()) return true;
////        if (reference.isGlobal()) return true;
//
//        String faceName = Name.typeName(reference.name().toString());
//        if (faceName.isEmpty()) return false;
//
//        if (Name.isMetaNamed(faceName)) {
//            faceName = Name.asMetaMember(faceName);
//        }
//
//        String packageName = Name.packageName(reference.fullName());
//        if (!packageName.isEmpty()) {
//            Typified face = faceFrom(reference);
//            if (face != null) return true;
//        }
//
//        boolean result = faces.containsKey(faceName);
//        if (result) {
////            System.out.println("Library resolved " + symbol);
//        }
//        else {
////            System.out.println("Library can't resolve " + faceName + " from " + reference.name());
//        }
//        return result;
//    }

    /**
     * @return the type name of the variable to which a reference resolves.
     * @param reference refers to a variable in some compiler scope.
     */
//    public String resolveTypeName(Named reference) {
//        Class faceClass = resolveType(reference);
//        if (faceClass != null) {
//            if (Mirror.forClass(faceClass).hasMetaclass()) {
//                return MetaclassType().fullName();
//            } else {
//                return faceClass.getCanonicalName();
//            }
//        }
//
//        String symbol = Name.typeName(reference.name().toString());
//        if (faces.containsKey(symbol)) {
//            return MetaclassType().fullName();
//        }
//        return RootType().fullName();
//    }

    public static final String ClassFileType = ".class";
    public static final String SourceFileType = ".hoot";
    public static FilenameFilter SourceFileFilter =
        ((File dir, String name) -> name.endsWith(SourceFileType));

    public static final String TargetFileType = ".java";
    public static FilenameFilter TargetFileFilter =
        ((File dir, String name) -> name.endsWith(TargetFileType));

} // Library
