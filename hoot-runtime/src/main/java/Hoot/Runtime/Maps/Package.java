package Hoot.Runtime.Maps;

import java.io.*;
import java.util.*;

import Hoot.Runtime.Names.Name;
import Hoot.Runtime.Faces.Named;
import Hoot.Runtime.Names.TypeName;
import Hoot.Runtime.Behaviors.Mirror;
import Hoot.Runtime.Behaviors.Typified;
import Hoot.Runtime.Emissions.Item;
import Hoot.Runtime.Faces.UnitFile;
import Hoot.Runtime.Faces.Logging;
import static Hoot.Runtime.Functions.Utils.*;
import static Hoot.Runtime.Maps.Library.*;
import static Hoot.Runtime.Maps.ClassPath.*;
import static Hoot.Runtime.Names.Keyword.*;
import static Hoot.Runtime.Names.Operator.Dot;
import static Hoot.Runtime.Names.Primitive.Blank;
import static Hoot.Runtime.Names.Primitive.Dollar;
import static Hoot.Runtime.Behaviors.HootRegistry.*;
import static Hoot.Runtime.Exceptions.ExceptionBase.*;
import static Hoot.Runtime.Names.Name.removeTail;

/**
 * A package of class definitions.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 1999,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class Package implements Named, Logging {

    public Package() { this(Empty); }
    public Package(String packageName) { super(); name = packageName; }
    public Package(String packageName, String folderPath) { this(packageName); baseFolder = folderPath; }

    String name;
    @Override public String name() { return name; }
    public void name(String packageName) { name = packageName; }
    public String pathname() { return name.replace(Dot, Slash); }
    public String parentName() { return Name.packageName(name); }
    public static Package named(String packageName) { return CurrentLib.packageNamed(nameFrom(packageName)); }

    public static final String[] LiteralPackages = { "Hoot.Behaviors", "Hoot.Magnitudes", "Hoot.Collections" };
    public static final List<String> LiteralPackageNames = wrap(LiteralPackages);
    public boolean definesLiterals() { return LiteralPackageNames.contains(name()); }
    public boolean definesMagnitudes() { return LiteralPackages[1].equals(name()); }
    public boolean definesCollections() { return LiteralPackages[2].equals(name()); }
    public boolean definesBehaviors() { return (LiteralPackages[0].equals(name()) || definesSmalltalk()); }
    public boolean definesSmalltalk() { return Smalltalk.equals(name()); }
    public boolean definesRoot() { return RootPackages[0].equals(name()); }

    public static UnitFile.Factory UnitFactory;
    private UnitFile createUnit(String fullName) { return UnitFactory.createUnit(fullName, name()); }
    private Map<String, UnitFile> createPeers() {
        HashMap<String, UnitFile> results = new HashMap();
        sourceFaces().forEach(f -> results.put(f, createUnit(f)));
        results.values().forEach(f -> f.peers(results));
        return results;
    }
    
//    private UnitFile parseSource(String fullName, List<String> peerFaces) throws Exception {
//        report("parsing " + fullName);
//        UnitFile result = UnitFactory.createUnit(fullName, name(), peerFaces);
//        result.parse();
//        return result;
//    }

    public HashMap<String, UnitFile> parseSources() throws Exception {
        HashMap<String, UnitFile> results = new HashMap();
        if (UnitFactory == null) return results;

        java.io.File sourceFolder = sourceFolder();
        if (!sourceFolder.exists()) {
            error("failed to locate sources for " + name());
            return results;
        }

        results.putAll(createPeers());
        results.values().forEach(f -> f.addStandardImports());
        results.values().forEach(f -> addFace((Typified)((Item)f).facialScope()));
        results.values().forEach(f -> f.parse());

        return results;
    }

//    public HashMap<String, UnitFile> parseSource(String faceName) throws Exception {
//        String fullName = name() + Dot + faceName;
//        if (faceName.startsWith(name())) {
//            fullName = faceName;
//        }
//
//        HashMap<String, UnitFile> results = new HashMap();
//        UnitFile fileScope = parseSource(fullName, sourceFaces());
//        results.put(fileScope.fullName(), fileScope);
//        return results;
//    }

    public List<String> sourceFaces() { return listFaces(sourceFolder(), SourceFileType, SourceFileFilter); }
    public List<String> targetFaces() { return listFaces(targetFolder(), TargetFileType, TargetFileFilter); }
    public List<String> listFaces(java.io.File folder, String type, FilenameFilter filter) {
        return map(wrap(folder.list(filter)), f -> removeTail(f, type)); }

    public File sourceFolder() { return new File(CurrentLib.sourcePath(), pathname()); }
    public File targetFolder() { return new File(CurrentLib.targetPath(), pathname()); }

    public File createTarget() {
        File targetFolder = targetFolder();

        if (!targetFolder.exists() && !targetFolder.mkdirs()) {
            error("Can't create " + targetFolder.getAbsolutePath());
            return null;
        }

        return targetFolder;
    }

    String baseFolder = Empty;
    public File directory() {
        return (baseFolder.isEmpty()) ? CurrentPath.locate(pathname()) : new File(baseFolder + pathname()); }

    public Set<String> packagedClassNames() { 
        Set<String> results = new HashSet();
        results.addAll(CurrentPath.classesInPackage(this));
        if (baseFolder.isEmpty() && definesRoot()) {
            results.add(JavaRoot().getSimpleName());
            results.addAll(wrap(RootExceptions));
        }

        return results; }

    public boolean needsLoad() { return packagedClassNames().size() > faceNames().size(); }
    public void loadFaces(List<String> faceNames) {
        if (faceNames != null && !faceNames.isEmpty()) {
            faceNames.forEach(faceName -> loadFace(faceName));
        }
    }

    public void loadFaces() {
        int count = faceNames().size();
        Set<String> faceNames = packagedClassNames();
        faceNames.forEach((faceName) -> {
            loadFace(faceName);
        });
        if (faceNames.size() > count) {
            whisper(name() + " loaded " + (faceNames.size() - count));
        }
    }

    @SuppressWarnings("UseSpecificCatch")
    public void loadFace(String shortName) {
        if (faces.get(shortName) != null) return;

        try {
            String fullName = qualify(shortName);
            Class c = Class.forName(fullName);
            addFace(Mirror.forClass(c));
        }
        catch (Throwable ex) {
            // ignore pathology
            whisper(Empty);
        }
    }

    Map<String, Typified> faces = new HashMap();
    public int countFaces() { return faces.size(); }
    public Set<String> faceNames() { return faces.keySet(); }
    public Map<String, Typified> knownFaces() { return new HashMap(faces); }
    public Typified faceNamed(String faceName) { return faces.get(Name.typeName(faceName)); }

    public void addFace(Typified face) {
        String fullName = face.fullName();
        String packageName = face.packageName();
        String typeName = Name.typeName(face.fullName());
        if (typeName.startsWith(packageName)) {
            typeName = typeName.substring(packageName.length() + 1);
        }

        faces.put(typeName, face);
        CurrentLib.addFace(face);
        registerMetaface(face);
        if (Name.isMetaNamed(fullName)) {
            packageMetaface(face);
        }
    }

    protected void registerMetaface(Typified face) {
        if (face.hasMetaface()) {
            Typified metaFace = face.$class();
            CurrentLib.addFace(metaFace);
            packageMetaface(metaFace);
        }
    }

    protected void packageMetaface(Typified metaFace) {
        if (metaFace.packageName().equals(name())) {
            addMetaface(metaFace);
        }
        else {
            Package.named(metaFace.packageName()).addMetaface(metaFace);
        }
    }

    protected void addMetaface(Typified metaFace) {
        String metaName = Name.typeName(metaFace.fullName());
        String packageName = Name.packageName(metaFace.fullName());
        if (metaName.startsWith(packageName)) {
            metaName = metaName.substring(packageName.length() + 1);
        }
        faces.put(metaName.replace(Dollar, Dot), metaFace);
        faces.put(metaName.replace(Dot, Dollar), metaFace);
    }

    public Map<String, TypeName> faceTypes() {
        HashMap<String, TypeName> results = new HashMap();
        faceNames().forEach((faceName) -> {
            results.put(faceName, TypeName.fromName(qualify(faceName)));
        });
        return results;
    }

    /**
     * @return whether the supplied (importName) ends with a wild card.
     * @param importName the name of an imported class, interface, or package.
     */
    public static boolean namesAllFaces(String importName) { return importName.endsWith(WildCard); }
    public static String nameFrom(String packageName) { return nameWithout(WildCard, packageName); }
    public static String nameWithout(String tail, String name) { return removeTail(name, tail); }

    public static String nameFrom(File baseFolder, File packageFolder) {
        return packageFolder.getPath()
                .substring(baseFolder.getPath().length())
                .replace(Slash, Blank).trim().replace(Blank, Dot); }

    public static final String WildCard = ".*";
    public static final String Slash = "/";

} // Package
