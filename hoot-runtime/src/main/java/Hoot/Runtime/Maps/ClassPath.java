package Hoot.Runtime.Maps;

import java.io.*;
import java.util.*;
import static java.util.Collections.reverse;

import Hoot.Runtime.Names.Name;
import Hoot.Runtime.Faces.Logging;
import static Hoot.Runtime.Functions.Utils.*;
import static Hoot.Runtime.Maps.Library.*;
import static Hoot.Runtime.Names.Operator.Dot;
import static Hoot.Runtime.Names.Primitive.*;

/**
 * Provides a directory of the classes located by the Java class path.
 * Locates packages by their directory names and provides a list of the classes contained in a package.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 1999,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class ClassPath implements Logging {

    protected ClassPath() { }

    public static void loadBasePaths(String... basePaths) {
        CurrentPath.loadPaths(map(wrap(basePaths), p -> new File(p))); }

    List<PathMap> contents = new ArrayList();
    void clear() { contents.clear(); }
    private List<PathMap> reversedPath() {
        List<PathMap> results = new ArrayList(contents);
        reverse(results);
        return results;
    }

    public void addMapped(File folder, boolean appended) {
        if (folder == null || !folder.exists()) return; // bail out

        PathMap map = buildMap(folder);
        if (appended) {
            contents.add(map);
        }
        else {
            contents.add(0, map);
        }
        map.load();
    }

    private PathMap buildMap(File folder) {
        return ZipMap.supports(folder.getAbsolutePath()) ?
                new ZipMap(folder.getAbsolutePath()) :
                new PathMap(folder.getAbsolutePath()) ; }

    /**
     * @param folder an element of a class path
     */
    public void mapPath(File folder) {
        boolean appended = true;
        addMapped(folder, appended);
        reportMapped(); // while mapping
    }

    static final String[] ConfiguredPaths = { "java.class.path", };
    private void mapStandardPaths() {
        wrap(ConfiguredPaths).forEach(path -> {
            for (String folderPath : systemValue(path).split(Separator)) 
                mapPath(new File(folderPath));
        });
    }

//    public void loadPaths(List<File> basePaths) { loadPaths(unwrap(basePaths, EmptyFiles)); }
    public void loadPaths(List<File> basePaths) { //  (3+ expected)
        reportMapping();

        // load up packages from the base paths
        clear();
        CurrentLib.configurePaths(basePaths);
        mapStandardPaths();        
        CurrentLib.loadBasePackages();
        CurrentLib.removeShadowedClasses();

        reverse(basePaths);
        basePaths.forEach(path -> { mapPath(path); });
        report(Empty);
    }

    /**
     * @return whether a named face can be located in the class path
     * @param fullName a fully qualified face name
     */
    public boolean canLocateFaceNamed(String fullName) {
        String packageName = Name.packageName(fullName);
        String faceName = Name.typeName(fullName);
        if (reversedPath().stream()
            .map((map) -> map.classesInFolder(packageName))
            .anyMatch((results) -> (!results.isEmpty() && results.contains(faceName)))) {
            return true;
        }

        if (reversedPath().stream()
            .anyMatch((map) -> (map.packageContaining(faceName) != null))) {
            return true;
        }

        return false;
    }

    /**
     * @return whether aPackage can be located in the class path
     * @param aPackage a potential Java/Hoot package
     */
    public boolean canLocatePackage(Package aPackage) {
        final String packagePath = aPackage.pathname();
        return matchAny(reversedPath(), (map) -> (!map.classesInFolder(packagePath).isEmpty())); }

    /**
     * @return the names of the classes contained in aPackage
     * @param aPackage a Java/Hoot package
     */
    public Set<String> classesInPackage(Package aPackage) {
        final String packagePath = aPackage.pathname();
        Set<String> result = new HashSet();
        reversedPath().forEach((map) -> {
            result.addAll(map.classesInFolder(packagePath));
        });
        return result;
    }

    /**
     * @return a package folder, or null
     * @param folder a relative folder name for a package
     */
    public java.io.File locate(String folder) {
        for (PathMap map : reversedPath()) {
            File result = map.locate(folder);
            if (result != null) {
                return result;
            }
        }
        return null;
    }

    public static String buildPath(String... basePaths) {
        int count = 0;
        StringBuilder builder = new StringBuilder();
        for (String basePath : basePaths) {
            if (count > 0) builder.append(Separator);
            builder.append(basePath);
            count++;
        }
        return builder.toString();
    }

    private void reportMapping() { System.out.print(MappingReport); }
    private void reportMapped() { System.out.print(Dot); }
    private void reportFailed(File path, Exception ex) {
        error(Empty);
        error(format(FailedReport, path.getAbsolutePath()), ex); }

    static final File[] EmptyFiles = { };
    static final String MappingReport = "mapping CLASSPATH ";
    static final String FailedReport = "failed to map %s";

    public static final String PathSeparator = "path.separator";
    public static final String Separator = systemValue(PathSeparator);
    public static final ClassPath CurrentPath = new ClassPath();
}
