### Smalltalk Type Library ###

| **Package** | **Contents** |
| ----------- | ------------ |
| Smalltalk Blocks | standard closures and predicates |
| Smalltalk Collections | standard collection protocols |
| Smalltalk Core | standard behavior protocols |
| Smalltalk Exceptions | standard exception protocols |
| Smalltalk Magnitudes | standard value protocols |
| Smalltalk Streams | standard stream protocols |

#### Notes

No surprise: some names are quite popular and descriptive for commonly found types.
To reduce the possibility of naming conflicts and keep some of its class references simple, 
Hoot maps some of the standard Smalltalk protocols to type names _different_ from those 
found in the [ANSI standard][st-ansi].
This applies especially to those types whose names also have potential conflicts 
with some found in the underlying Java type library.
These renamed types are emphasized in the table below with **bold**.

To simplify its implementation of the valuable protocols, `Hoot Runtime Blocks Enclosure` implements **all** the 
valuable protocols defined in section 5.4 of the standard. 
Also notice that `Transcript` becomes a class with **log** as its sole instance, so that the associated protocol
can be instantiated.

Many of the exception types and collection types defined in the standard are mapped directly to classes in Hoot, 
especially those whose names called out in the **Standard Globals** (5.2).

| **Section** | **ANSI Smalltalk** | **Hoot Smalltalk** |
| ----------- | ------------------ | ------------------ |
| _**5.3.1**_ | Object | Smalltalk Core **Subject** (Object) |
| 5.3.2 | nil | Smalltalk Core **Undefined** (Nil) |
| 5.3.3 | boolean | Smalltalk Core **Posit** (Boolean) |
| 5.3.4 | Character | Smalltalk Magnitudes **Code** (Character) |
| 5.3.5 | Character factory | Smalltalk Magnitudes **Code** type (Character class) |
| 5.3.6 | failedMessage | Smalltalk Core **SentMessage** (Message) |
| 5.3.7 | selector | Hoot Runtime Faces Selector (Symbol) |
| 5.3.8 | classDescription | Smalltalk Core **Classified** (Behavior) |
| 5.3.9 | instantiator | Smalltalk Core **Subject** type |
| 5.3.10 | Object class | Hoot Behaviors Object class |
| _**5.4.1**_ | valuable | Hoot Runtime Blocks Valuable + Arguable |
| 5.4.2 | niladicValuable | Hoot Runtime Blocks NiladicValuable <- Valuable |
| 5.4.3 | niladicBlock | Hoot Runtime Blocks Enclosure <- NiladicValuable |
| 5.4.4 | monadicValuable | Hoot Runtime Blocks MonadicValuable + Arguable |
| 5.4.5 | monadicBlock | Hoot Runtime Blocks Enclosure <- MonadicValuable |
| 5.4.6 | dyadicValuable | Hoot Runtime Blocks DyadicValuable + Arguable |
| _**5.5.1**_ | exceptionDescription | Smalltalk Exceptions ExceptionDescription |
| 5.5.2 | exceptionSignaler | Smalltalk Exceptions ExceptionSignaler |
| 5.5.3 | exceptionBuilder | Smalltalk Exceptions ExceptionBuilder |
| 5.5.4 | signaledException | Smalltalk Exceptions SignaledException |
| 5.5.5 | exceptionSelector | Smalltalk Exceptions ExceptionSelector |
| 5.5.6 | exceptionInstantiator | Hoot Exceptions Exception class |
| 5.5.7 | Exception class | Hoot Exceptions Exception class |
| 5.5.8 | Exception | Hoot Exceptions Exception |
| 5.5.9 | Notification class | Hoot Exceptions Notification class |
| 5.5.10 | Notification | Hoot Exceptions Notification |
| 5.5.11 | Warning class | Hoot Exceptions Warning class |
| 5.5.12 | Warning | Hoot Exceptions Warning |
| 5.5.13 | Error class | Hoot Exceptions Error class |
| 5.5.14 | Error | Hoot Exceptions Error |
| 5.5.15 | ZeroDivide factory | Hoot Exceptions ZeroDivide class |
| 5.5.16 | ZeroDivide | Hoot Exceptions ZeroDivide |
| 5.5.17 | MessageNotUnderstoodSelector | _not used_ |
| 5.5.18 | MessageNotUnderstood | Hoot Exceptions MessageNotUnderstood |
| 5.5.19 | exceptionSet | Hoot Exceptions ExceptionSet |
| _**5.6.1**_ | magnitude | Smalltalk Magnitudes **Scalar** |
| 5.6.2 | number | Smalltalk Magnitudes **Numeric** |
| 5.6.3 | rational | Smalltalk Magnitudes **Ratio** |
| 5.6.4 | Fraction | Smalltalk Magnitudes **Fractional** |
| 5.6.5 | integer | Smalltalk Magnitudes **Ordinal** |
| 5.6.6 | scaledDecimal | Smalltalk Magnitudes ScaledDecimal |
| 5.6.7 | Float | Smalltalk Magnitudes **Floater** |
| 5.6.8 | floatCharacterization | Smalltalk Magnitudes **Floater** type |
| 5.6.9 | Fraction factory | Smalltalk Magnitudes **Fractional** |
| _**5.7.1**_ | collection | Smalltalk Collections **Collected** |
| 5.7.2 | abstractDictionary | Smalltalk Collections **CollectedDictionary** |
| 5.7.3 | Dictionary | Hoot Collections Dictionary |
| 5.7.4 | IdentityDictionary | Hoot Collections IdentityDictionary |
| 5.7.5 | extensibleCollection | Smalltalk Collections **CollectedVariably** |
| 5.7.6 | Bag | Smalltalk Collections **CollectedBaggage** |
| 5.7.7 | Set | Smalltalk Collections **CollectedDistinctly** |
| 5.7.8 | sequencedReadableCollection | Smalltalk Collections **CollectedSequentially** |
| 5.7.9 | Interval | Smalltalk Collections **OrdinalRange** |
| 5.7.10 | readableString | Smalltalk Collections ReadableString |
| 5.7.11 | symbol | Hoot Collections Symbol |
| 5.7.12 | sequencedCollection | Smalltalk Collections **CollectedSequentially** |
| 5.7.13 | String | Smalltalk Collections **MutableString** |
| 5.7.14 | Array | Smalltalk Collections **CollectedArray** |
| 5.7.15 | ByteArray | Hoot Collections ByteArray |
| 5.7.16 | sequencedContractibleCollection | Smalltalk Collections **CollectedCollapsibly** |
| 5.7.17 | SortedCollection | Smalltalk Collections **CollectedSortably** |
| 5.7.18 | OrderedCollection | Smalltalk Collections **CollectedOrdinally** |
| 5.7.19 | Interval factory | Smalltalk Collections **OrdinalRange** type |
| 5.7.20 | collection factory | Smalltalk Collections **Collected** type |
| 5.7.21 | Dictionary factory | Smalltalk Collections **CollectedDictionary** type |
| 5.7.22 | IdentityDictionary factory | Hoot Collections IdentityDictionary class |
| 5.7.23 | initializableCollection factory | Smalltalk Collections  |
| 5.7.24 | Array factory | Smalltalk Collections **CollectedArray** type |
| 5.7.25 | Bag factory | Smalltalk Collections **CollectedBaggage** type |
| 5.7.26 | ByteArray factory | Hoot Collections ByteArray class |
| 5.7.27 | OrderedCollection factory | Smalltalk Collections **CollectedOrdinally** type |
| 5.7.28 | Set factory | Smalltalk Collections **CollectedDistinctly** type |
| 5.7.29 | SortedCollection factory | Smalltalk Collections **CollectedSortably** type |
| 5.7.30 | String factory | Hoot Collections String class |
| _**5.9.1**_ | sequencedStream | Smalltalk Streams **StreamedSequence** |
| 5.9.2 | gettableStream | Smalltalk Streams **StreamedSource** |
| 5.9.3 | collectionStream | Hoot Streams CollectionStream |
| 5.9.4 | puttableStream | Smalltalk Streams **StreamedSink** |
| 5.9.5 | ReadStream | Hoot Streams ReadStream |
| 5.9.6 | WriteStream | Hoot Streams WriteStream |
| 5.9.7 | ReadWriteStream | Smalltalk Streams **StreamStore** |
| 5.9.8 | Transcript | Hoot Streams Transcript log |
| 5.9.9 | ReadStream factory | Hoot Streams ReadStream class |
| 5.9.10 | ReadWriteStream factory | Smalltalk Streams StreamStore type |
| 5.9.11 | WriteStream factory | Hoot Streams WriteStream class |
| 5.10.1 | FileStream | Hoot Streams FileStream |
| 5.10.2 | readFileStream | Smalltalk Streams **FileStreamReader** |
| 5.10.3 | writeFileStream | Smalltalk Streams **FileStreamWriter** |
| 5.10.4 | FileStream factory | Hoot Streams FileStream class |

```
Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
```
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for LICENSE details.


[st-ansi]: https://web.archive.org/web/20060216073334/http://www.smalltalk.org/versions/ANSIStandardSmalltalk.html

