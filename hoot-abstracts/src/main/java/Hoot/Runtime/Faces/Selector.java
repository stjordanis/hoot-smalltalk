package Hoot.Runtime.Faces;

/**
 * A method selector. Defines the type signature for ANSI Smalltalk Selector (section 5.3.7).
 * Implementors of this marker are expected to override value and count.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public interface Selector extends Named, Valued {

    default boolean isOptimized() { return false; }
    @Override default String name() { return this.toString(); }

    /**
     * Finds the class whose fully qualified name is the value of this selector.
     * @return a Class, or null
     */
    default Class<?> toClass() {
        try {
            return Class.forName(name());
        }
        catch (ClassNotFoundException e) {
            return null;
        }
    }

} // Selector
