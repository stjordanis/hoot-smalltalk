package Hoot.Runtime.Faces;

/**
 * A value.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public interface Valued extends Logging {

    default <R> R value() { return (R)this; }
    default <R> R as(Class<R> aType) { return (R)this; }
    default Class<?> valueType() { return getClass(); }

    /**
     * A value meta-type. A marker type for all meta-types.
     */
    public static interface Metatype { }

} // Valued
