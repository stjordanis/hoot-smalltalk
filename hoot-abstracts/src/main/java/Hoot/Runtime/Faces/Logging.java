package Hoot.Runtime.Faces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

/**
 * Grafts standard logging methods onto any class.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public interface Logging {

    default Logger logger() { return LoggerFactory.getLogger(getClass()); }

    default void whisper(String message) { logger().debug(message); }
    default void report(String message) { logger().info(message); }
    default void warn(String message) { logger().warn(message); }
    default void error(String message) { logger().error(message); }
    default void error(String message, Throwable ex) { logger().error(message, ex); }
    default String format(String report, Object ... values) { return String.format(report, values); }

    static int countMatches(CharSequence seq, CharSequence chars) { return StringUtils.countMatches(seq, chars); }
    static String strip(String value, String chars) { return StringUtils.strip(value, chars); }

    static boolean notEmpty(CharSequence seq) { return !isEmpty(seq); }
    static boolean isEmpty(CharSequence seq) { return StringUtils.isEmpty(seq); }

    static final String Empty = "";

} // Logging
