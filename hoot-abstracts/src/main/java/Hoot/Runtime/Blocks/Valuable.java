package Hoot.Runtime.Blocks;

/**
 * A basic block closure protocol.
 * Defines the type signature for ANSI Smalltalk Valuable (section 5.4.1).
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public interface Valuable extends Arguable {

    /**
     * @return the result of this block when evaluated
     * @param <R> a result type
     */
    public <R> R value();

} // Valuable
