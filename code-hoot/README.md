### Hoot Class Library ###

Contains code for the Hoot library classes located in [src/main/Hoot](src/main/Hoot).
[CompilerTest](src/test/java/Hoot/Compiler/CompilerTest.java) runs during a build, and calls 
the Hoot compiler to generate Java classes in **libs-hoot** for the Hoot classes contained in this library.


| **Package** | **Contents** |
| ----------- | ------------ |
| Hoot Behaviors | essential behaviors: `Object`, `Behavior`, `Class`, `Boolean`, etc. |
| Hoot Collections | various standard collections: `Array`, `Set`, `Dictionary`, `OrderedCollection`, etc. |
| Hoot Exceptions | standard and extended exceptions: `Error`, `ZeroDivide`, etc. |
| Hoot Geometry | simple geometry: `Point`, `Rectangle` |
| Hoot Magnitudes | standard and literal values: `SmallInteger`, `Fraction`, `Fixed`, `Float`, etc. |
| Hoot Streams | standard streams: `Transcript`, `FileStream`, `CollectionStream`, etc. |

#### Notes

To ease integration with the underlying platform library, some of the standard Hoot classes wrap an underlying platform class.
Here are a few representative examples:

| **Hoot** | **Java** |
| -------- | -------- |
| Set | java.util.HashSet |
| Dictionary | java.util.HashMap |
| OrderedCollection | java.util.ArrayList |
| IdentityDictionary | java.util.IdentityHashMap |
| IdentitySet | java.util.IdentityHashMap |
| String | java.lang.StringBuffer |
| Symbol | java.lang.String (**intern**-ed) |
| MetaclassBase | java.lang.Class |


```
Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
```
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for LICENSE details.
